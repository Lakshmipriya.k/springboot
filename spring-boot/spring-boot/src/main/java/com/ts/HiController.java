package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {

	@RequestMapping("hi")	//hi - URL
	public String sayHi() {
		return "hi from HiController";
	}
	
	@RequestMapping("welcome")	//welcome - URL
	public String welcome() {
		return "Welcome to SpringBoot";
	}
	@RequestMapping("bye")	//welcome - URL
	public String bye() {
		return "bye to SpringBoot";
	}
	
}

