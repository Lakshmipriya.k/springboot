package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployeeDao;
import com.model.Employee;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeDao empDAO;
	
	@GetMapping("getEmployees")
	public List<Employee> getEmployees() {
		return empDAO.getEmployees();
	}
	
	@GetMapping("getEmployeeById/{id}")
	public Employee getEmployeeById(@PathVariable("id") int empId) {
		return empDAO.getEmployeeById(empId);
	}
	@GetMapping("getLogin/{email}/{password}")
	public Employee getLogin(@PathVariable("email") String emailId,@PathVariable("password") String password) {
		return empDAO.getLogin(emailId,password);
	}
	@GetMapping("getEmployeeByName/{name}")
	public Employee getEmployeeByName(@PathVariable("name") String empName) {
		return empDAO.getEmployeeByName(empName);
	}
	@PostMapping("addEmployee")
	public Employee addProduct(@RequestBody Employee employee) {
		return empDAO.addEmployee(employee);
	}
	
	@PutMapping("updateemployee")
	public Employee updateemployee(@RequestBody Employee employee) {
		return empDAO.updateemployee(employee);
	}
	
	@DeleteMapping("deleteEmployeeById/{id}")
	public String deleteEmployeeById(@PathVariable("id") int empId) {
		empDAO.deleteEmployeeById(empId);
		return "Product with ProductId: " + empId + ", Deleted Successfully";
	}
}
	

