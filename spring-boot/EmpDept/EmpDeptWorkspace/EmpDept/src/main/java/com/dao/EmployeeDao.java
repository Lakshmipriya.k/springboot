package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeDao {
	
	@Autowired
	EmployeeRepository empRepo;

	public List<Employee> getEmployees() {
		return empRepo.findAll();
	}
	public Employee getLogin(String emailId, String password) {
		return empRepo.getLogin(emailId,password);
	}
	public Employee getEmployeeById(int empId) {
		return empRepo.findById(empId).orElse(null);
	}

	public Employee getEmployeeByName(String empName) {
		return empRepo.findByName(empName);
	}
	public Employee addEmployee(Employee employee) {
		return empRepo.save(employee);
	}

	public Employee updateemployee(Employee employee) {
		return empRepo.save(employee);
	}
	public void deleteEmployeeById(int empId) {
		empRepo.deleteById(empId);
	}



}
