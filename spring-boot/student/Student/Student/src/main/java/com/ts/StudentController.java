package com.ts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDAO;
import com.model.Student;

@RestController
public class StudentController {
	@Autowired
	StudentDAO studDAO;

	
	@GetMapping("getStudent")
	public Student getStudent(){
		Student student = new Student();
		student.setStudentId(101);
		student.setStudentName("Sresta");
		student.setStudentCourse("Java");
		student.setStudentFees(45000.00);
		return student;
	}
	
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents(){
		
		List<Student> studentList = new ArrayList<Student>();
		Student student1 = new Student(101, "sresta","Java", 45000.00);
		Student student2 = new Student(101, "yamu","Python", 45000.00);
		Student student3 = new Student(101, "jasu","SQL", 45000.00);
		Student student4 = new Student(101, "mamatha","Html", 45000.00);
		Student student5 = new Student(101, "lahari","Oracle", 45000.00);
		
		studentList.add(student1);
		studentList.add(student2);
		studentList.add(student3);
		studentList.add(student4);
		studentList.add(student5);
		
		return studentList;
	}
	
	@GetMapping("getStudents")
	public List<Student> getStudents() {
		List<Student> studList = studDAO.getStudents();
		return studList;
	}
	
	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int studId) {
		Student student = studDAO.getStudentById(studId);
		return student;
	}
	
	@GetMapping("getStudentByName/{name}")
	public Student getStudentByName(@PathVariable("name") String studName) {
		return studDAO.getStudentByName(studName);
	}
	
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student) {
		return studDAO.addStudent(student);
	}
	
	@PutMapping("updateStudent")
	public Student updateStudent(@RequestBody Student student) {
		return studDAO.updateStudent(student);
	}
	
	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int studId) {
		studDAO.deleteStudentById(studId);
		return "Student with StudentId: " + studId + ", Deleted Successfully";
	}
	
	

	

}