package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity



public class Student {
    @Id@GeneratedValue
    private int studentId;
    
    @Column(name="studName")
    private String studentName;
    private String studentCourse;
    private double studentFees;
   
    public Student() {
        super();
    }

    public Student(int studentId, String studentName, String studentCourse, double studentFees) {
        super();
        this.studentId = studentId;
        this.studentName = studentName;
        this.studentCourse = studentCourse;
        this.studentFees = studentFees;
    }
    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentCourse() {
        return studentCourse;
    }

    public void setStudentCourse(String studentCourse) {
        this.studentCourse = studentCourse;
    }

    public double getStudentFees() {
        return studentFees;
    }

    public void setStudentFees(double studentFees) {
        this.studentFees = studentFees;
    }

    @Override
    public String toString() {
        return "Student [studentId=" + studentId + ", studentName=" + studentName +
                ", studentCourse=" + studentCourse + ", studentFees=" + studentFees + "]";
    }
}
